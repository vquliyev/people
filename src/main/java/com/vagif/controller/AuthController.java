package com.vagif.controller;


import com.vagif.dto.request.LoginRq;
import com.vagif.dto.respond.LoginRs;
import com.vagif.dto.respond.RegisterRs;
import com.vagif.model.Person;
import com.vagif.service.AuthService;
import com.vagif.utils.CurrentPerson;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
public class AuthController {

    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/login")
    public LoginRs handleLogin(@RequestBody LoginRq rq) {
        return authService.login(rq.getName(), rq.getPassword(), rq.isRemember())
                .map(t -> new LoginRs("OK", t, CurrentPerson.get()))
                .orElse(new LoginRs("ERR", null, null));
    }

    @PostMapping("/register")
    public RegisterRs handleRegister(@RequestBody Person person) {
        boolean result = authService.registerNew(person);
        return result ? RegisterRs.ok() : RegisterRs.alreadyExists();
    }

    @PutMapping("/person")
    public LoginRs handlePut(@RequestBody Person person) {
        return authService.updatePerson(person)
                .map(t -> new LoginRs("OK", t, CurrentPerson.get()))
                .orElse(new LoginRs("ERR", null, null));
    }

}
