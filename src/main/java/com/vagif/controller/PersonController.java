package com.vagif.controller;

import com.vagif.model.Person;
import com.vagif.service.PersonService;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api")
public class PersonController {

    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/persons")
    List<Person> getAllPersons(){
        return personService.getAllPersons();
    }

    @GetMapping("/persons/find")
    List<Person> findPersonByName(@RequestParam String name){
        return personService.findPersonByName(name);
    }

    @PostMapping("/persons")
    Person createPerson(@RequestBody Person person){
        return personService.createPerson(person);
    }

    @PutMapping("/persons/{id}")
    Person createPerson(@RequestBody Person person, @PathVariable Long id){
        return personService.updatePerson(id, person);
    }

    @GetMapping("/persons/{id}")
    Person getPersonById(@PathVariable Long id){
        return personService.getPersonById(id);
    }

}
