package com.vagif.dto.request;


import lombok.Data;

@Data
public class LoginRq {

    private String name;
    private String password;
    private boolean remember;

}
