package com.vagif.dto.respond;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RegisterRs {

    private String message;

    public static RegisterRs ok() {
        return new RegisterRs("Ok");
    }

    public static RegisterRs alreadyExists() {
        return new RegisterRs("Error: User Already Exists");
    }

}
