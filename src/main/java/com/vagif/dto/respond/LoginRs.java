package com.vagif.dto.respond;


import com.vagif.model.Person;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginRs {

    private String message;
    private String token;
    private Person person;

}
