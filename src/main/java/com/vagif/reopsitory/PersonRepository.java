package com.vagif.reopsitory;

import com.vagif.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Optional;


public interface PersonRepository extends JpaRepository<Person, Long> {
    @Query("SELECT p FROM Person p WHERE p.name = :name")
    List<Person> searchPersonForName(@Param("name") String name);
    Optional<Person> findByName(@NotNull @Size(min = 1, message = "This field can't be empty.") String name);

}
