package com.vagif.security.userDetails;

import com.vagif.model.Person;
import com.vagif.reopsitory.PersonRepository;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Configuration
public class UserDetailsServiceJpa implements UserDetailsService {

    private PersonRepository personRepository;

    public UserDetailsServiceJpa(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public static UserDetails mapper(Person person) {
        return new MyUserDetails(
                person.getId(),
                person.getName(),
                person.getPassword(),
                person.getRoles()
        );
    }

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {

        return personRepository.findByName(name)
                .map(UserDetailsServiceJpa::mapper)
                .orElseThrow(() -> new UsernameNotFoundException(
                        String.format("Person `%s` not found", name
                )));

    }

    public UserDetails loadUserById(long person_id) throws UsernameNotFoundException {
        return personRepository.findById(person_id)
                .map(UserDetailsServiceJpa::mapper)
                .orElseThrow(()->new UsernameNotFoundException(
                        String.format("Person with id:%d` not found", person_id)));
    }

}
