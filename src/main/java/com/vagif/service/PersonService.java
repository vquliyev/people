package com.vagif.service;

import com.vagif.model.Person;
import com.vagif.reopsitory.PersonRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class PersonService {

    private final PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> getAllPersons() {
        return personRepository.findAll();
    }

    public Person getPersonById(Long id) {
        Optional<Person> person = personRepository.findById(id);
        return person.isPresent() ? person.get() : null;
    }

    public Person createPerson(Person person) {
        return personRepository.save(person);
    }

    public Person updatePerson(Long id, Person person) {
        Optional<Person> oldPerson = personRepository.findById(id);
        if(oldPerson.isPresent()){
            return personRepository.save(person);
        }
        return null;
    }

    public List<Person> findPersonByName(String name) {
        return personRepository.searchPersonForName(name);
    }
}
