package com.vagif.service;


import com.vagif.model.Person;
import com.vagif.reopsitory.PersonRepository;
import com.vagif.security.jwt.Const;
import com.vagif.security.jwt.JwtTokenService;
import com.vagif.security.userDetails.MyUserDetails;
import com.vagif.utils.CurrentPerson;
import com.vagif.utils.NullAwareBeanUtilsBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

@Service
public class AuthService {

    private final AuthenticationManager am;
    private final JwtTokenService tp;
    private final PersonRepository repo;
    private final PasswordEncoder enc;

    public AuthService(AuthenticationManager am, JwtTokenService tp, PersonRepository repo, PasswordEncoder enc) {
        this.am = am;
        this.tp = tp;
        this.repo = repo;
        this.enc = enc;
    }

    public boolean registerNew(Person person) {
        Optional<Person> found = repo.findByName(person.getName());
        if (!found.isPresent()) {
            String[] roles = {"USER"};
            person.setRoles(roles);
            person.setPassword(enc.encode(person.getPassword()));
            repo.save(person);
        }
        return !found.isPresent();
    }

    public Optional<String> login(String name, String password, boolean rememberMe) {
        return Optional.of(am.authenticate(new UsernamePasswordAuthenticationToken(name, password)))
                .filter(Authentication::isAuthenticated)
                .map(authentication -> {
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                    return authentication;
                })
                .map(authentication -> (MyUserDetails) authentication.getPrincipal())
                .map(ud -> tp.generateToken(ud.getId(), rememberMe))
                .map(t -> Const.AUTH_BEARER + t);
    }

    public Optional<String> updatePerson(Person person) {
        Optional<Person> previous_person = repo.findById(CurrentPerson.get().getId());
        return previous_person.map(p -> {
            NullAwareBeanUtilsBean bean = new NullAwareBeanUtilsBean();
            try {
                bean.copyProperties(p, person);
                if (person.getPassword() != null) p.setPassword(enc.encode(person.getPassword()));
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
            repo.save(p);
            return p;
        })
                .flatMap(p -> login(p.getName(), p.getPassword(), false));
    }
}
