package com.vagif.utils;

import com.vagif.model.Person;
import com.vagif.reopsitory.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

public class CurrentPerson {

    private static PersonRepository personRepository;

    @Autowired
    public CurrentPerson(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public static Person get() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String name = authentication.getName();
        return personRepository.findByName(name).get();
    }


}
